Betterlock Plus - applet for the Linux Cinnamon desktop

Provides up to five<sup>[1]</sup> interactive icons placed in the system tray:

- Num Lock
- Caps Lock
- Scroll Lock<sup>[2]</sup>
- Display Backlight _(laptops only)_
- Keyboard Backlight _(untested)_

First three icons light up according to the state of their corresponding keys on the keyboard. They can each be clicked on to toggle their state.

Display Backlight icon is scrollable only, providing quick change in the laptop's display luminosity (native luminosity, not software brightness). Its function is identical to pressing the keyboard combinations for Brightness Up or Brightness Down.

Keyboard Backlight icon is both clickable and scrollable, providing easy way to turn keyboard light on/off and/or alter its intensity. However, due to lack of proper hardware this function has never been tested, therefore it may function erroneously or not at all. Feedback would be appreciated, a piece of hardware for testing purposes - even more.

There is an additional button in settings that allows locking all icons in read-only mode, meaning they will not be interactive anymore, only providing visual feedback on the state of the keys without being able to alter their state or change display/keyboard lights.

Icon size, panel arrangement, colors, as well as tooltip font size and popup notifications are also customizable in the Settings dialog.

<hr />


<sup>[1]</sup> Depending on the actual hardware the OS and applet are running on not all functions/icons would be available.

<sup>[2]</sup> For the Scroll Lock key to function in Linux some changes would have to be made to the keyboard layout(s) used in the system. I'm not aware of the reason why this key has been disabled, or what its use may be in this time and age; nevertheless, for those who might want to play with it there should be some tutorials on the web as to how exactly to modify the keyboard layout file(s).

© Drugwash, 2022-2023
